package co.zip.candidate.userapi.repository

import co.zip.candidate.userapi.model.AccountEntity
import co.zip.candidate.userapi.model.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface AccountRepository : JpaRepository<AccountEntity, Long> {
    fun findByUser(user: UserEntity): Optional<AccountEntity>
}
