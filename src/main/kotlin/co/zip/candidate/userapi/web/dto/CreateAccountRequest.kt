package co.zip.candidate.userapi.web.dto

import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

data class CreateAccountRequest(
        @field:Min(1)
        @field:NotNull
        val userId: Long?,

        @field:NotNull
        @field:Pattern(regexp = "(?i)PREMIUM|FREE")
        val subscriptionType: String?
)
