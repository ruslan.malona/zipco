package co.zip.candidate.userapi.web

import co.zip.candidate.userapi.service.exceptions.AccountExistsException
import co.zip.candidate.userapi.service.exceptions.InsufficientFundsException
import co.zip.candidate.userapi.service.exceptions.UserExistsException
import co.zip.candidate.userapi.service.exceptions.UserNotFoundException
import org.springframework.http.ResponseEntity
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.util.*


@ControllerAdvice
class ErrorHandler {
    @ExceptionHandler(UserNotFoundException::class)
    fun handleValidationException(e: UserNotFoundException): ResponseEntity<Any> {
        val errors = listOf(e.message!!)
        return ResponseEntity.status(404).body(buildBadRequestBody(errors))
    }

    @ExceptionHandler(InsufficientFundsException::class)
    fun handleValidationException(e: InsufficientFundsException): ResponseEntity<Any> {
        val errors = listOf(e.message!!)
        return ResponseEntity.status(400).body(buildBadRequestBody(errors))
    }

    @ExceptionHandler(UserExistsException::class)
    fun handleValidationException(e: UserExistsException): ResponseEntity<Any> {
        val errors = listOf(e.message!!)
        return ResponseEntity.status(400).body(buildBadRequestBody(errors))
    }

    @ExceptionHandler(AccountExistsException::class)
    fun handleValidationException(e: AccountExistsException): ResponseEntity<Any> {
        val errors = listOf(e.message!!)
        return ResponseEntity.status(400).body(buildBadRequestBody(errors))
    }

    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleValidationException(e: MethodArgumentNotValidException): ResponseEntity<Any> {
        //Get all errors
        val errors: List<String> = e.bindingResult.allErrors
                .map{
                    if (it is FieldError) {
                        "Parameter '${it.field}' has invalid value '${it.rejectedValue}' : ${it.defaultMessage}"
                    } else {
                        "Object '${it.objectName}' has failed validation due to '${it.defaultMessage}'"
                    }
                }

        return ResponseEntity.status(400).body(buildBadRequestBody(errors))
    }

    private fun buildBadRequestBody(errors: List<String>): Map<String, Any> {
        val body: MutableMap<String, Any> = LinkedHashMap()
        body["timestamp"] = Date()
        body["errors"] = errors
        return body
    }
}
