package co.zip.candidate.userapi.web.dto

import co.zip.candidate.userapi.service.dto.User
import javax.validation.constraints.Email
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank

class CreateUserRequest(
        @field:NotBlank
        val name: String?,

        @field:Email
        val email: String?,

        @field:Min(1)
        val salary: Int?,

        @field:Min(1)
        val expenses: Int?
) {
    fun toUser() = User(null, name!!, email!!, salary!!, expenses!!)
}
