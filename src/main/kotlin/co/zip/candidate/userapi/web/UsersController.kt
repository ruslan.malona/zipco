package co.zip.candidate.userapi.web

import co.zip.candidate.userapi.service.UserService
import co.zip.candidate.userapi.web.dto.CreateUserRequest
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/users")
class UsersController(private val userService: UserService) {

    @GetMapping
    fun findAll() = userService.getAll()

    @PostMapping
    fun create(@Valid @RequestBody userRequest: CreateUserRequest) = userService.create(userRequest.toUser())

    @GetMapping("/{userId}")
    fun findUser(@PathVariable userId: Long) = userService.getOne(userId)

}
