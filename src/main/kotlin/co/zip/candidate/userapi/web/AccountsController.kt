package co.zip.candidate.userapi.web

import co.zip.candidate.userapi.service.AccountService
import co.zip.candidate.userapi.web.dto.CreateAccountRequest
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/accounts")
class AccountsController(private val accountService: AccountService) {

    @GetMapping
    fun findAll() = accountService.getAll()

    @PostMapping
    fun create(@Valid @RequestBody accountRequest: CreateAccountRequest) = accountService.create(accountRequest)
}
