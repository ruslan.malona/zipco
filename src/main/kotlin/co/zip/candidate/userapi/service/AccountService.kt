package co.zip.candidate.userapi.service

import co.zip.candidate.userapi.model.AccountEntity
import co.zip.candidate.userapi.model.SubscriptionType
import co.zip.candidate.userapi.repository.AccountRepository
import co.zip.candidate.userapi.repository.UserRepository
import co.zip.candidate.userapi.service.dto.Account
import co.zip.candidate.userapi.service.exceptions.AccountExistsException
import co.zip.candidate.userapi.service.exceptions.InsufficientFundsException
import co.zip.candidate.userapi.service.exceptions.UserNotFoundException
import co.zip.candidate.userapi.web.dto.CreateAccountRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface AccountService {
    fun create(account: CreateAccountRequest): Account
    fun getAll(): List<Account>
}

@Service
class AccountServiceImpl(
        private val accountRepository: AccountRepository,
        private val userRepository: UserRepository
) : AccountService {

    @Transactional
    override fun create(account: CreateAccountRequest): Account {
        val userEntity = userRepository.findById(account.userId!!)
                .orElseThrow { throw UserNotFoundException("User not found by id ${account.userId}") }

        if (userEntity.salary - userEntity.expenses < 1000)
            throw InsufficientFundsException("User ${account.userId} doesn't have enough profit")

        accountRepository.findByUser(userEntity)
                .ifPresent { throw AccountExistsException("User ${account.userId} already has account") }

        val accountEntity = AccountEntity(userEntity, SubscriptionType.valueOf(account.subscriptionType!!))
        val savedEntity = accountRepository.save(accountEntity)
        return Account.fromEntity(savedEntity)
    }

    override fun getAll(): List<Account> {
        val accountEntities = accountRepository.findAll()
        return accountEntities.map { Account.fromEntity(it) }
    }
}
