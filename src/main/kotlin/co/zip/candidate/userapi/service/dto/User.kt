package co.zip.candidate.userapi.service.dto

import co.zip.candidate.userapi.model.UserEntity

data class User(
        val id: Long?,
        val name: String,
        val email: String,
        val salary: Int,
        val expenses: Int
) {
    fun toEntity() = UserEntity(name, email, salary, expenses)

    companion object {
        fun fromEntity(entity: UserEntity) = User(entity.id, entity.name, entity.email, entity.salary, entity.expenses)
    }
}
