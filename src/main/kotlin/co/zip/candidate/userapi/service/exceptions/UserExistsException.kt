package co.zip.candidate.userapi.service.exceptions

class UserExistsException(message: String) : RuntimeException(message)
