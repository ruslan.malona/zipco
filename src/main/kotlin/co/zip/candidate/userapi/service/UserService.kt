package co.zip.candidate.userapi.service

import co.zip.candidate.userapi.repository.UserRepository
import co.zip.candidate.userapi.service.dto.User
import co.zip.candidate.userapi.service.exceptions.UserExistsException
import co.zip.candidate.userapi.service.exceptions.UserNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface UserService {
    fun getAll(): List<User>
    fun getOne(id: Long): User?
    fun create(user: User): User
}

@Service
class UserServiceImpl(
        private val userRepository: UserRepository
) : UserService {
    override fun getAll(): List<User> {
        val userEntities = userRepository.findAll()
        return userEntities.map { User.fromEntity(it) }
    }

    override fun getOne(id: Long): User {
        val userEntity = userRepository.findById(id)
                .orElseThrow { throw UserNotFoundException("User not found by id $id") }
        return User.fromEntity(userEntity)
    }

    @Transactional
    override fun create(user: User): User {
        userRepository.findByEmail(user.email)
                .ifPresent { throw UserExistsException("User ${user.email} already exists") }
        val entity = user.toEntity()
        val savedEntity = userRepository.save(entity)
        return User.fromEntity(savedEntity)
    }
}
