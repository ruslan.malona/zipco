package co.zip.candidate.userapi.service.dto

import co.zip.candidate.userapi.model.AccountEntity
import co.zip.candidate.userapi.model.SubscriptionType
import java.time.LocalDateTime

data class Account(
        val user: User,
        val subscriptionType: SubscriptionType,
        val created: LocalDateTime
) {
    companion object {
        fun fromEntity(entity: AccountEntity) =
                Account(User.fromEntity(entity.user), entity.subscriptionType, entity.created)
    }
}

