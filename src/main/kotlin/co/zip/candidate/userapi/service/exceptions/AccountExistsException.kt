package co.zip.candidate.userapi.service.exceptions

class AccountExistsException(message: String) : RuntimeException(message)
