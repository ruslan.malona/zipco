package co.zip.candidate.userapi.service.exceptions

class InsufficientFundsException(message: String) : RuntimeException(message)
