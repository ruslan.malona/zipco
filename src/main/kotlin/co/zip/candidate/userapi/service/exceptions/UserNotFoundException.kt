package co.zip.candidate.userapi.service.exceptions

class UserNotFoundException(message: String) : RuntimeException(message)
