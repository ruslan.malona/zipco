package co.zip.candidate.userapi.model

import javax.persistence.*

@Entity
@Table(name = "users")
data class UserEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long?,

        val name: String,

        val email: String,

        val salary: Int,

        val expenses: Int
) {
    constructor(name: String, email: String, salary: Int, expenses: Int) : this(null, name, email, salary, expenses)
}
