package co.zip.candidate.userapi.model

enum class SubscriptionType {
        FREE, PREMIUM
}
