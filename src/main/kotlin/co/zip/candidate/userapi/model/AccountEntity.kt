package co.zip.candidate.userapi.model

import java.time.LocalDateTime
import java.time.ZoneOffset
import javax.persistence.*

@Entity
@Table(name = "accounts")
data class AccountEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long?,

        val subscriptionType: SubscriptionType,

        @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
        @JoinColumn(name = "user_id", nullable = false)
        val user: UserEntity
) {
        constructor(user: UserEntity, subscriptionType: SubscriptionType) : this(null, subscriptionType, user)
        constructor(user: UserEntity, subscriptionType: SubscriptionType, created: LocalDateTime) : this(null, subscriptionType, user) {
                this.created = created
        }

        var created: LocalDateTime = LocalDateTime.now(ZoneOffset.UTC)
}
