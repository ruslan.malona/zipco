CREATE TABLE IF NOT EXISTS `users`
(
    `id`       int         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name`     varchar(50) NOT NULL,
    `email`    varchar(50) NOT NULL UNIQUE,
    `salary`   int         NOT NULL,
    `expenses` int         NOT NULL
);

