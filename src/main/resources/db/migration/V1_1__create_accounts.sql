CREATE TABLE IF NOT EXISTS `accounts`
(
    `id`                int                 NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `user_id`           int                 NOT NULL,
    `subscription_type` tinyint   default 0 NOT NULL comment '0 - Free, 1 - Premium',
    `created`           timestamp,
    CONSTRAINT accounts_users_fk FOREIGN KEY (user_id) references users (id)
);
