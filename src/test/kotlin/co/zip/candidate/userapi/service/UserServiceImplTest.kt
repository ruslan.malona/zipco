package co.zip.candidate.userapi.service

import co.zip.candidate.userapi.model.UserEntity
import co.zip.candidate.userapi.repository.UserRepository
import co.zip.candidate.userapi.service.dto.User
import co.zip.candidate.userapi.service.exceptions.UserExistsException
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
class UserServiceImplTest {
    @Mock
    lateinit var userRepository: UserRepository

    @InjectMocks
    lateinit var userService: UserServiceImpl

    private val userEntity = UserEntity("name", "email", 2000, 100)
    private val user = User(null,"name", "email", 2000, 100)

    @Test
    fun `should return all users`() {
        Mockito.`when`(userRepository.findAll()).thenReturn(listOf(userEntity))

        val users = userService.getAll()

        Assertions.assertThat(users).containsExactly(user)
    }

    @Test
    fun `should return one user`() {
        Mockito.`when`(userRepository.findById(1L)).thenReturn(Optional.of(userEntity))

        val foundUser = userService.getOne(1L)

        Assertions.assertThat(foundUser).isEqualTo(user)
    }

    @Test
    fun `should save user`() {
        Mockito.`when`(userRepository.findByEmail("email")).thenReturn(Optional.empty())
        Mockito.`when`(userRepository.save(userEntity)).thenReturn(userEntity)

        userService.create(user)

        Mockito.verify(userRepository).save(userEntity)
    }

    @Test
    fun `should throw exception if user exist`() {
        Mockito.`when`(userRepository.findByEmail("email")).thenReturn(Optional.of(userEntity))

        assertThrows<UserExistsException> { userService.create(user) }
    }
}
