package co.zip.candidate.userapi.service

import co.zip.candidate.userapi.model.AccountEntity
import co.zip.candidate.userapi.model.SubscriptionType
import co.zip.candidate.userapi.model.UserEntity
import co.zip.candidate.userapi.repository.AccountRepository
import co.zip.candidate.userapi.repository.UserRepository
import co.zip.candidate.userapi.service.dto.Account
import co.zip.candidate.userapi.service.dto.User
import co.zip.candidate.userapi.service.exceptions.AccountExistsException
import co.zip.candidate.userapi.service.exceptions.InsufficientFundsException
import co.zip.candidate.userapi.service.exceptions.UserNotFoundException
import co.zip.candidate.userapi.web.dto.CreateAccountRequest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import java.time.LocalDateTime
import java.util.*

@ExtendWith(MockitoExtension::class)
class AccountServiceImplTest {
    @Mock
    lateinit var accountRepository: AccountRepository
    
    @Mock
    lateinit var userRepository: UserRepository
    
    @InjectMocks
    lateinit var accountService: AccountServiceImpl
    
    private val userEntity = UserEntity(1L, "name", "email", 2000, 100)
    private val accountEntity = AccountEntity(userEntity, SubscriptionType.PREMIUM, LocalDateTime.now())
    private val user = User(1L,"name", "email", 2000, 100)
    private val account = Account(user, SubscriptionType.PREMIUM, accountEntity.created)

    @Test
    fun `should return all accounts`() {
        Mockito.`when`(accountRepository.findAll()).thenReturn(listOf(accountEntity))

        val accounts = accountService.getAll()
        
        assertThat(accounts).containsExactly(account)
    }

    @Test
    fun `should create new account`() {
        Mockito.`when`(userRepository.findById(1)).thenReturn(Optional.of(userEntity))
        Mockito.`when`(accountRepository.findByUser(userEntity)).thenReturn(Optional.empty())
        Mockito.`when`(accountRepository.save(AccountEntity(userEntity, SubscriptionType.PREMIUM))).thenReturn(accountEntity)

        accountService.create(CreateAccountRequest(1L, "PREMIUM"))

        Mockito.verify(accountRepository).save(Mockito.anyObject())
    }

    @Test
    fun `should throw exception if user not found`() {
        Mockito.`when`(userRepository.findById(1)).thenReturn(Optional.empty())

        assertThrows<UserNotFoundException> { accountService.create(CreateAccountRequest(1L, "PREMIUM")) }
    }

    @Test
    fun `should throw exception if has not sufficient funds`() {
        val userEntity = UserEntity("name", "email", 1000, 100)
        Mockito.`when`(userRepository.findById(1)).thenReturn(Optional.of(userEntity))

        assertThrows<InsufficientFundsException> { accountService.create(CreateAccountRequest(1L, "PREMIUM")) }
    }

    @Test
    fun `should throw exception if account already exists`() {
        Mockito.`when`(userRepository.findById(1)).thenReturn(Optional.of(userEntity))
        Mockito.`when`(accountRepository.findByUser(userEntity)).thenReturn(Optional.of(accountEntity))

        assertThrows<AccountExistsException> { accountService.create(CreateAccountRequest(1L, "PREMIUM")) }
    }
}
