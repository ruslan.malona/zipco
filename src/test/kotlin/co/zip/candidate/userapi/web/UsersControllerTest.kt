package co.zip.candidate.userapi.web

import co.zip.candidate.userapi.service.UserService
import co.zip.candidate.userapi.service.dto.User
import co.zip.candidate.userapi.service.exceptions.UserExistsException
import co.zip.candidate.userapi.service.exceptions.UserNotFoundException
import org.hamcrest.CoreMatchers
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers


@ExtendWith(SpringExtension::class)
@WebMvcTest(controllers = [UsersController::class])
@AutoConfigureMockMvc
internal class UsersControllerTest {
    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var userService: UserService

    @Test
    fun `Should list all users`() {
        Mockito.`when`(userService.getAll()).thenReturn(listOf(User(1L,"name", "email", 10, 1)))
        mvc.perform(MockMvcRequestBuilders.get("/api/users"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.`is`("name")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].email", CoreMatchers.`is`("email")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary", CoreMatchers.`is`(10)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].expenses", CoreMatchers.`is`(1)))
    }

    @Test
    fun `Should get one user`() {
        Mockito.`when`(userService.getOne(1)).thenReturn(User(1L,"name", "email", 10, 1))
        mvc.perform(MockMvcRequestBuilders.get("/api/users/1"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.`is`("name")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.`is`("email")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary", CoreMatchers.`is`(10)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.expenses", CoreMatchers.`is`(1)))
    }

    @Test
    fun `Should return not found error if user not found`() {
        Mockito.`when`(userService.getOne(1)).thenThrow(UserNotFoundException("not found"))
        mvc.perform(MockMvcRequestBuilders.get("/api/users/1"))
                .andExpect(MockMvcResultMatchers.status().`is`(404))
    }

    @Test
    fun `Should create new user`() {
        mvc.perform(MockMvcRequestBuilders.post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"name" : "name", "email" : "email@example.com", "salary" : 1000, "expenses" : 100}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().isOk)
        Mockito.verify(userService).create(User(null,"name", "email@example.com", 1000, 100))
    }

    @Test
    fun `Should return validation error if email exists`() {
        Mockito.`when`(userService.create(User(null,"name", "email@example.com", 1000, 100)))
                .thenThrow(UserExistsException("user exists"))

        mvc.perform(MockMvcRequestBuilders.post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"name" : "name", "email" : "email@example.com", "salary" : 1000, "expenses" : 100}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().`is`(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors[0]", CoreMatchers.`is`("user exists")))
    }

    @Test
    fun `Should return validation error if email incorrect`() {
        mvc.perform(MockMvcRequestBuilders.post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"name" : "name", "email" : "email.com", "salary" : 1000, "expenses" : 100}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().`is`(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors[0]", CoreMatchers.`is`("Parameter 'email' has invalid value 'email.com' : must be a well-formed email address")))
    }

    @Test
    fun `Should return validation error if name not provided`() {
        mvc.perform(MockMvcRequestBuilders.post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"email" : "email@example.com", "salary" : 1000, "expenses" : 100}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().`is`(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors[0]", CoreMatchers.`is`("Parameter 'name' has invalid value 'null' : must not be blank")))
    }

    @Test
    fun `Should return validation error if salary 0`() {
        mvc.perform(MockMvcRequestBuilders.post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"name" : "name", "email" : "email@example.com", "salary" : 0, "expenses" : 100}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().`is`(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors[0]", CoreMatchers.`is`("Parameter 'salary' has invalid value '0' : must be greater than or equal to 1")))
    }

    @Test
    fun `Should return validation error if expenses 0`() {
        mvc.perform(MockMvcRequestBuilders.post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"name" : "name", "email" : "email@example.com", "salary" : 1000, "expenses" : 0}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().`is`(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors[0]", CoreMatchers.`is`("Parameter 'expenses' has invalid value '0' : must be greater than or equal to 1")))
    }
}
