package co.zip.candidate.userapi.web

import co.zip.candidate.userapi.model.SubscriptionType
import co.zip.candidate.userapi.service.AccountService
import co.zip.candidate.userapi.service.dto.Account
import co.zip.candidate.userapi.service.dto.User
import co.zip.candidate.userapi.service.exceptions.AccountExistsException
import co.zip.candidate.userapi.service.exceptions.InsufficientFundsException
import co.zip.candidate.userapi.service.exceptions.UserNotFoundException
import co.zip.candidate.userapi.web.dto.CreateAccountRequest
import org.hamcrest.CoreMatchers
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.LocalDateTime

@ExtendWith(SpringExtension::class)
@WebMvcTest(controllers = [AccountsController::class])
@AutoConfigureMockMvc
class AccountsControllerTest {
    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var accountService: AccountService

    @Test
    fun `Should list all accounts`() {
        val user = User(1L,"name", "email", 10, 1)
        Mockito.`when`(accountService.getAll()).thenReturn(listOf(Account(user, SubscriptionType.PREMIUM, LocalDateTime.now())))
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].user.name", CoreMatchers.`is`("name")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].subscriptionType", CoreMatchers.`is`("PREMIUM")))
    }

    @Test
    fun `Should create an account`() {
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"userId" : 1, "subscriptionType" : "PREMIUM"}
                """.trimIndent()))
        .andExpect(MockMvcResultMatchers.status().isOk)
        Mockito.verify(accountService).create(CreateAccountRequest(1L, "PREMIUM"))
    }

    @Test
    fun `Should return not found error if userId not found`() {
        Mockito.`when`(accountService.create(anyObject())).thenThrow(UserNotFoundException("not found"))
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"userId" : 1, "subscriptionType" : "PREMIUM"}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().`is`(404))
    }

    @Test
    fun `Should return validation error if userId has insufficient funds`() {
        Mockito.`when`(accountService.create(anyObject())).thenThrow(InsufficientFundsException("not found"))
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"userId" : 1, "subscriptionType" : "PREMIUM"}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().`is`(400))
    }

    @Test
    fun `Should return validation error if already has account`() {
        Mockito.`when`(accountService.create(anyObject())).thenThrow(AccountExistsException("account exists"))
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"userId" : 1, "subscriptionType" : "PREMIUM"}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().`is`(400))
    }

    @Test
    fun `Should return validation error if userId absent`() {
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"subscriptionType" : "PREMIUM"}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().`is`(400))
        Mockito.verify(accountService, Mockito.never()).create(anyObject())
    }

    @Test
    fun `Should return validation error if subscriptionType absent`() {
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"userId" : 1}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().`is`(400))
        Mockito.verify(accountService, Mockito.never()).create(anyObject())
    }

    @Test
    fun `Should return validation error if subscriptionType unknown`() {
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {"userId" : 1, "subscriptionType" : "FREEMIUM"}
                """.trimIndent()))
                .andExpect(MockMvcResultMatchers.status().`is`(400))
        Mockito.verify(accountService, Mockito.never()).create(anyObject())
    }

    private fun <T> anyObject(): T {
        return Mockito.anyObject<T>()
    }
}
