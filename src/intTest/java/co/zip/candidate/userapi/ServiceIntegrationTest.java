package co.zip.candidate.userapi;

import co.zip.candidate.userapi.service.dto.Account;
import co.zip.candidate.userapi.service.dto.User;
import co.zip.candidate.userapi.web.dto.CreateAccountRequest;
import co.zip.candidate.userapi.web.dto.CreateUserRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {ServiceIntegrationTest.Initializer.class})
class ServiceIntegrationTest {
    @Container
    private static final MySQLContainer MY_SQL_CONTAINER = new MySQLContainer("mysql:8");

    @Autowired
    private TestRestTemplate restTemplate;

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + MY_SQL_CONTAINER.getJdbcUrl(),
                    "spring.datasource.username=" + MY_SQL_CONTAINER.getUsername(),
                    "spring.datasource.password=" + MY_SQL_CONTAINER.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @Test
    void shouldAddUser() {
        var userRequest = new CreateUserRequest("Ruslan", "my@email1.com", 10000, 1000);
        var response = restTemplate.postForEntity("/api/users", userRequest, User.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getId()).isNotNull();

        var received = restTemplate.getForEntity("/api/users/" + response.getBody().getId(), User.class);
        Assertions.assertEquals(response.getBody(), received.getBody());
    }

    @Test
    void shouldNotAllowAddingUserWithSameEmail() {
        var userRequest1 = new CreateUserRequest("Ruslan", "my@email2.com", 10000, 1000);
        var response = restTemplate.postForEntity("/api/users", userRequest1, User.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getId()).isNotNull();

        var userRequest2 = new CreateUserRequest("Ivan", "my@email2.com", 10000, 1000);
        var errorResponse = restTemplate.postForEntity("/api/users", userRequest2, Void.class);
        assertThat(errorResponse.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void shouldCreateAccount() {
        var userRequest = new CreateUserRequest("Ruslan", "my@email3.com", 10000, 1000);
        var userResponse = restTemplate.postForEntity("/api/users", userRequest, User.class);
        assertThat(userResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(userResponse.getBody()).isNotNull();
        assertThat(userResponse.getBody().getId()).isNotNull();

        var accountRequest = new CreateAccountRequest(userResponse.getBody().getId(), "PREMIUM");
        var accountResponse = restTemplate.postForEntity("/api/accounts", accountRequest, Account.class);
        assertThat(accountResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(accountResponse.getBody()).isNotNull();
        assertThat(accountResponse.getBody().getUser()).isNotNull();
        assertThat(accountResponse.getBody().getUser().getId()).isEqualTo(userResponse.getBody().getId());
    }

    @Test
    void shouldNotCreateAccountIfFundsInsufficient() {
        var userRequest = new CreateUserRequest("Ruslan", "my@email4.com", 1000, 100);
        var userResponse = restTemplate.postForEntity("/api/users", userRequest, User.class);
        assertThat(userResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(userResponse.getBody()).isNotNull();
        assertThat(userResponse.getBody().getId()).isNotNull();

        var accountRequest = new CreateAccountRequest(userResponse.getBody().getId(), "PREMIUM");
        var accountResponse = restTemplate.postForEntity("/api/accounts", accountRequest, Void.class);
        assertThat(accountResponse.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }
}
