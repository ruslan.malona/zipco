# User API Dev Guide

## REST API
### Create user
URL: `POST http://localhost:8080/api/users`

Headers: `Content-Type: application/json`

Body: 
```
{"name" : "<user_name>","email" :  "<user_email>", "salary" :  <number>, "expenses" :  <number>}
```

### Get single user
URL: `GET http://localhost:8080/api/users/<user_id>`

### Get all users
URL: `GET http://localhost:8080/api/users`

### Create account
URL: `POST http://localhost:8080/api/accounts`

Headers: `Content-Type: application/json`

Body: 
```
{"userId" : "<user_id>","subscriptionType" : "<FREE|PREMIUM>"}
```
### Get all accounts
URL: `GET http://localhost:8080/api/users`

## Software requirements
- Java 11
- Docker

## Dependencies
App has single dependency on MySQL server. It can be run via docker-compose. See below

## Building
To build project on local machine run gradle command:

`./gradlew clean build`

Or you can import the project in your favourite IDE and build it there

## Testing
`./gradlew clean test` - will run unit test

Also, I have prepared an Integration test which starts up the application and uses real DB.
To run it execute `./gradlew clean integrationTest` or run `ServiceIntegrationTest` from IDE

## Developing
First of all, need to start MySQL server. This can be done via command:
`docker-compose -f docker-compose-local.yml up`

To run application you can just run main class `UserApiApplication` from IDE.
Or alternatively via gradle: `./gradlew bootRun`

## Deploying
To deploy app, first need to build docker image:
`./gradlew bootBuildImage --imageName=zipco/userapi`

Then you can run app via docker-compose:
`docker-compose -f docker-compose-prod.yml up`
